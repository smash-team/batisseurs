import Worker from "./worker"

export default class Building {
  constructor(object) {
    this.id = object.id
    this.reward = object.reward
    this.victoryPoint = object.victoryPoint
    this.stone = object.stone
    this.wood = object.wood
    this.knowledge = object.knowledge
    this.done = object.done
    this.workers = object.workers
  }

  serialize() {
    return {
      id: this.id,
      reward: this.reward,
      victoryPoint: this.victoryPoint,
      stone: this.stone,
      wood: this.wood,
      knowledge: this.knowledge,
      done: this.done,
      workers: this.workers.map(worker => worker.serialize())
    }
  }

  static create(object) {
    object.stoneProduced = 0
    object.woodProduced = 0
    object.knowledgeProduced = 0
    object.tileProduced = 0
    object.workers = []
    return Building.deserialize(object)
  }

  static deserialize(object) {
    object.workers = object.workers.map(worker => Worker.deserialize(worker))
    return new Building(object)
  }
}
