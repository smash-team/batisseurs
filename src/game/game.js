import Worker from "./worker"
import Building from "./building"
import Player from "./player"
import uuidv4 from "uuid/v4"
import shuffle from "lodash/shuffle"
import { importWorkers, importBuildings } from "../services/cardService"

function arrayWithout(array, elem) {
  return array.filter(e => e !== elem)
}

export default class Game {
  constructor(object) {
    this.id = object.id
    this.createDate = object.createDate
    this.nbPlayers = object.nbPlayers
    this.players = object.players
    this.name = object.name
    this.currentPlayer = object.currentPlayer
    this.moneyAvailable = object.moneyAvailable
    this.workers = object.workers
    this.buildings = object.buildings
    this.remainingWorkers = object.remainingWorkers
    this.remainingBuildings = object.remainingBuildings
    this.nextWorker = object.nextWorker
    this.nextBuilding = object.nextBuilding
    this.done = object.done

    this._workersDeck = object._workersDeck
    this._buildingsDeck = object._buildingsDeck
  }

  output() {
    const self = this.serialize()
    const out = {}
    for (const property in self) {
      if (property[0] !== "_") out[property] = self[property]
    }
    return out
  }

  serialize() {
    return {
      id: this.id,
      createDate: this.createDate,
      nbPlayers: this.nbPlayers,
      players: this.players.map(player => player.serialize()),
      name: this.name,
      currentPlayer: this.currentPlayer,
      moneyAvailable: this.moneyAvailable,
      workers: this.workers.map(worker => worker.serialize()),
      buildings: this.buildings.map(building => building.serialize()),
      remainingWorkers: this.remainingWorkers,
      remainingBuildings: this.remainingBuildings,
      nextWorker: this.nextWorker.serialize(),
      nextBuilding: this.nextBuilding.serialize(),
      done: this.done,

      _workersDeck: this._workersDeck,
      _buildingsDeck: this._buildingsDeck
    }
  }

  static async create(nbPlayers, name, doShuffle) {
    // Handle default values
    if (doShuffle === undefined) doShuffle = true

    const players = [1, 2, 3, 4].slice(0, nbPlayers).map(Player.create)

    let workersDeck = (await importWorkers()).map(worker =>
      Worker.create(worker)
    )
    const buildingsDeck = (await importBuildings()).map(building =>
      Building.create(building)
    )

    // Give initial apprentices
    const apprentices = workersDeck.filter(worker => worker.isApprentice())
    if (doShuffle) {
      shuffle(apprentices)
    }
    players.forEach(player => {
      const apprentice = apprentices.pop()
      player.obtainWorker(apprentice)
      workersDeck = arrayWithout(workersDeck, apprentice)
    })

    if (doShuffle) {
      shuffle(workersDeck)
      shuffle(buildingsDeck)
    }

    // Draw 5 cards from the buildings deck, those are the visible ones
    const availableBuildings = buildingsDeck.splice(0, 5)
    // Draw 5 cards from the workers deck, those are the visible ones
    const availableWorkers = workersDeck.splice(0, 5)

    const game = new Game({
      id: uuidv4(),
      createDate: Date.now(),
      nbPlayers: nbPlayers,
      players: players,
      name: name,
      currentPlayer: 1, // Player 1 always starts
      moneyAvailable: 40,
      workers: availableWorkers,
      buildings: availableBuildings,
      remainingWorkers: workersDeck.length,
      remainingBuildings: buildingsDeck.length,
      nextWorker: workersDeck[0],
      nextBuilding: buildingsDeck[0],
      done: false,

      _workersDeck: workersDeck,
      _buildingsDeck: buildingsDeck
    })

    players.forEach(player => game.playerRecieveMoney(player.id, 10))
    game.beginTurn()

    return game
  }

  static deserialize(object) {
    object.players = object.players.map(player => Player.deserialize(player))
    object.workers = object.workers.map(worker => Worker.deserialize(worker))
    object.buildings = object.buildings.map(building =>
      Building.deserialize(building)
    )
    object.nextWorker = Worker.deserialize(object.nextWorker)
    object.nextBuilding = Building.deserialize(object.nextBuilding)
    return new Game(object)
  }

  beginTurn() {
    this.players[this.currentPlayer - 1].beginTurn()
  }

  nextTurn() {
    if (this.currentPlayer === this.nbPlayers) this.currentPlayer = 0
    this.currentPlayer++
    this.beginTurn()
  }

  playerRecieveMoney(playerID, amount) {
    if (this.moneyAvailable < amount)
      throw new Error("Not enough money in bank")
    this.moneyAvailable -= amount
    this.players[playerID - 1].recieveMoney(amount)
  }

  playerLoseMoney(playerID, amount) {
    this.players[playerID - 1].loseMoney(amount)
  }

  playerTakeBuilding(playerID, buildingID) {
    const index = this.buildings.find(building => building.id === buildingID)
    if (index === -1) throw new Error("Cannot pick not-available building")

    this.players[playerID - 1].useActions(1)

    // Give the player the card, remove it from the available ones, and add a new one
    this.players[playerID - 1].beginConstruction(
      this.buildings.splice(index, 1, this.nextBuilding)[0]
    )
    // Make the top deck card visible
    this.nextBuilding = this._buildingsDeck.pop()
  }
}
