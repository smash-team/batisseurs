export default class Worker {
  constructor(object) {
    this.id = object.id
    this.price = object.price
    this.stone = object.stone
    this.wood = object.wood
    this.knowledge = object.knowledge
    this.tile = object.tile
  }

  serialize() {
    return {
      id: this.id,
      price: this.price,
      stone: this.stone,
      wood: this.wood,
      knowledge: this.knowledge,
      tile: this.tile
    }
  }

  static create(object) {
    return Worker.deserialize(object)
  }

  static deserialize(object) {
    return new Worker(object)
  }

  isApprentice() {
    return this.price === 2
  }
}
