export default class Player {
  constructor(object) {
    this.id = object.id
    this.finishedBuildings = object.finishedBuildings
    this.availableWorkers = object.availableWorkers
    this.underConstructionBuildings = object.underConstructionBuildings
    this.money = object.money
    this.victoryPoints = object.victoryPoints
    this.actions = object.actions
  }

  serialize() {
    return {
      id: this.id,
      finishedBuildings: this.finishedBuildings,
      availableWorkers: this.availableWorkers,
      underConstructionBuildings: this.underConstructionBuildings,
      money: this.money,
      victoryPoints: this.victoryPoints,
      actions: this.actions
    }
  }

  static create(id) {
    return Player.deserialize({
      id: id,
      finishedBuildings: [],
      availableWorkers: [],
      underConstructionBuildings: [],
      money: 0,
      victoryPoints: 0,
      actions: 0
    })
  }

  static deserialize(object) {
    return new Player(object)
  }

  beginTurn() {
    this.actions = 3
  }

  obtainActions(nbActions) {
    this.actions += nbActions
  }

  useActions(nbActions) {
    if (this.actions < nbActions) throw new Error("Not enough actions!")
    this.actions -= nbActions
  }

  obtainWorker(worker) {
    this.availableWorkers.push(worker)
  }

  beginConstruction(building) {
    this.underConstructionBuildings.push(building)
  }

  recieveMoney(amount) {
    this.money += amount
  }

  loseMoney(amount) {
    if (this.money < amount) throw new Error("Not enough money")
    this.money -= amount
  }
}
