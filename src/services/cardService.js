import camelCase from "lodash/camelCase"
import path from "path"
import readFile from "../utils/readFile"

export function _parseCSV(csv) {
  const lines = csv.split("\n")

  // Read first line to get key names
  const properties = lines[0].split(";")

  // Read remaining lines to generate object
  return lines.slice(1).map(line => {
    const entry = {}
    line.split(";").forEach((value, i) => {
      entry[camelCase(properties[i])] = Number.parseInt(value)
    })
    return entry
  })
}

export async function importBuildings() {
  const file = await readFile(
    path.join(__dirname, "/../ressources/buildings.csv")
  )
  return _parseCSV(file)
}

export async function importWorkers() {
  const file = await readFile(
    path.join(__dirname, "/../ressources/workers.csv")
  )
  return _parseCSV(file)
}
