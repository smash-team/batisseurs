import Game from "../game/game"
import path from "path"
import writeFile from "../utils/writeFile"
import readFile from "../utils/readFile"
import HttpError from "../middlewares/HttpError"

export async function createGame(request) {
  if (request.numberOfPlayers === undefined || request.name === undefined)
    throw new Error("Missing required parameter")
  const game = await Game.create(
    request.numberOfPlayers,
    request.name,
    request.shuffle
  )

  // Write the game to storage
  await writeFile(
    path.join(__dirname, `../../storage/game_${game.id}.json`),
    JSON.stringify(game.serialize())
  )

  return game.output()
}

export async function viewGame(gameId) {
  const filePath = path.join(__dirname, `../../storage/game_${gameId}.json`)
  try {
    const file = await readFile(filePath)
    return JSON.parse(file)
  } catch (e) {
    throw new Error("Game file doesn't exist")
  }
}

export async function playAction(gameId, playerId, body) {
  const filePath = path.join(__dirname, `../../storage/game_${gameId}.json`)
  let file
  try {
    file = await readFile(filePath)
  } catch (e) {
    throw new Error("Game file doesn't exist")
  }
  const game = Game.deserialize(JSON.parse(file))

  if (playerId !== game.currentPlayer)
    throw new HttpError(401, "Please wait until your turn!")

  switch (body.type) {
    case "TAKE_BUILDING":
      game.playerTakeBuilding(playerId, body.payload.buildingId)
      break
    case "TAKE_WORKER":
    case "TAKE_MONEY":
    case "SEND_WORKER":
    case "BUY_ACTION":
      throw new Error("Action not implemented yet.")

    case "END_TURN":
      game.nextTurn()
      break

    default:
      throw new HttpError(400, "Invalid action.")
  }

  // Write the game to storage
  await writeFile(
    path.join(__dirname, `../../storage/game_${game.id}.json`),
    JSON.stringify(game.serialize())
  )

  return game.output()
}
