import * as cardService from "./cardService"

describe("_parseCSV", () => {
  test("Empty CSV", async () => {
    const ret = await cardService._parseCSV("")
    expect(ret).toStrictEqual([])
  })

  test("Sample CSV", async () => {
    const ret = await cardService._parseCSV(`\
col1;col2;col3
0;1;2
42;684;638416498649\
`)
    expect(ret).toStrictEqual([
      { col1: 0, col2: 1, col3: 2 },
      { col1: 42, col2: 684, col3: 638416498649 }
    ])
  })
})
