import * as gamesService from "./gamesService"
import writeFile from "../utils/writeFile"
jest.mock("../utils/writeFile")

describe("createGame", () => {
  test("Create a game", async () => {
    // Don't create an actual game, though
    writeFile.mockResolvedValue(new Promise((resolve, reject) => resolve()))
    expect(async () => {
      await gamesService.createGame({
        numberOfPlayers: 2,
        name: "Test game",
        shuffle: false
      })
    }).not.toThrow()
  })
})

describe("viewGame", () => {
  test("Invalid game Id", async () => {
    await expect(gamesService.viewGame("Invalid Id")).rejects.toThrow(
      "Game file doesn't exist"
    )
  })
})
