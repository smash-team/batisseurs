import fs from "fs"
import { dirname } from "path"

export default async function writeFile(fileName, data) {
  await fs.promises.mkdir(dirname(fileName), { recursive: true })

  return new Promise((resolve, reject) => {
    try {
      fs.writeFile(fileName, data, "utf8", err => {
        if (err) throw err
        resolve()
      })
    } catch (err) {
      reject(err)
    }
  })
}
