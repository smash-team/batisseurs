import readFile from "./readFile"
import fs from "fs"
jest.mock("fs")

fs.readFile.mockImplementation((path, _opt, callback) => {
  if (path !== "noExist") callback(null, "Not empty")
  else throw new Error("File not found")
})

describe("readFile", () => {
  test("Non-empty", async () => {
    const ret = await readFile("notEmpty")
    expect(ret).toStrictEqual("Not empty")
  })

  test("Does not exist", async () => {
    await expect(readFile("noExist")).rejects.toThrow("File not found")
  })
})
