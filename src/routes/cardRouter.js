import { importBuildings, importWorkers } from "../services/cardService"
import express from "express"
import HttpError from "../middlewares/HttpError"
const router = express.Router()

router.get("/buildings", async (req, res) => {
  try {
    res.json(await importBuildings())
  } catch (e) {
    throw new HttpError(500, "Can't read building cards.")
  }
})

router.get("/workers", async (req, res) => {
  try {
    res.json(await importWorkers())
  } catch (e) {
    throw new HttpError(500, "Can't read worker cards.")
  }
})

export default router
