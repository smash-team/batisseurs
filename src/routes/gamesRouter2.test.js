import request from "supertest"
import app from "../app"
import readFile from "../utils/readFile"
jest.mock("../utils/readFile")

describe("GET /games/{gameId}", () => {
  test("GET with wrong gameId should return 500", async () => {
    const response = await request(app).get("/games/wrongId")
    expect(response.statusCode).toBe(500)
    expect(response.text).toEqual("Can't read game details.")
  })
  test("the GET method should respond", async () => {
    const testGameFile = `{"id":"676761cd-b655-49ec-b985-87304c44eadc","createDate":1573886252283,"nbPlayers":4}`
    readFile.mockResolvedValue(testGameFile)
    const response = await request(app).get("/games/goodId")
    expect(response.statusCode).toBe(200)
    expect(response.body).toStrictEqual({
      id: "676761cd-b655-49ec-b985-87304c44eadc",
      createDate: 1573886252283,
      nbPlayers: 4
    })
  })
})
