import { createGame, viewGame, playAction } from "../services/gamesService"
import express from "express"
import HttpError from "../middlewares/HttpError"
const router = express.Router()

router.post("/", async (req, res) => {
  try {
    res.json(await createGame(req.body))
  } catch (e) {
    console.error(e)
    throw new HttpError(500, "Can't create game.")
  }
})

router.get("/:gameId", async (req, res) => {
  try {
    res.json(await viewGame(req.params.gameId))
  } catch (e) {
    throw new HttpError(500, "Can't read game details.")
  }
})

router.post("/:gameId/actions", async (req, res) => {
  try {
    res.json(
      await playAction(req.params.gameId, req.headers["player-id"], req.body)
    )
  } catch (e) {
    if (e instanceof HttpError) throw e
    throw new HttpError(400, "Can't play action.")
  }
})

export default router
