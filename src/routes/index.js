import healthRouter from "./healthRouter"
import cardsRouter from "./cardRouter"
import gamesRouter from "./gamesRouter"
import express from "express"

const router = express.Router()

router.use("/health", healthRouter)
router.use("/cards", cardsRouter)
router.use("/games", gamesRouter)

export default router
