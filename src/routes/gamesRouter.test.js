import request from "supertest"
import app from "../app"
import writeFile from "../utils/writeFile"
jest.mock("../utils/writeFile")

describe("POST /games", () => {
  test("POST should create a new game", async () => {
    // Don't create an actual game, though
    writeFile.mockResolvedValue(new Promise((resolve, reject) => resolve()))
    const response = await request(app)
      .post("/games")
      .send({
        numberOfPlayers: 2,
        shuffle: false,
        name: "Test game"
      })
    expect(response.statusCode).toBe(200)
  })

  test("POST without params should error out", async () => {
    const response = await request(app).post("/games")
    expect(response.statusCode).toBe(500)
  })
})
